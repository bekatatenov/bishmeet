package com.bishmeet.bishmeet.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public abstract class BaseEntity {
    @Id
    private Long id;
    @Column(name = "started_at")
    private LocalDateTime startedAt;
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;
}
