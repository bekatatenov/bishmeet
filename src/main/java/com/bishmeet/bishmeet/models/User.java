package com.bishmeet.bishmeet.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User extends BaseEntity {
    private String username;
    private String email;
    @Column(name = "user_image")
    private String image;
    @Column(name = "phone_number")
    private Integer phoneNumber;
    private String password;
    private boolean enabled;
    @Builder.Default
    private String role = "User";
    @Column(name = "is_social")
    private boolean isSocial;
}
