package com.bishmeet.bishmeet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BishmeetApplication {

    public static void main(String[] args) {
        SpringApplication.run(BishmeetApplication.class, args);
    }

}
