package com.bishmeet.bishmeet.repositories;

import com.bishmeet.bishmeet.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
