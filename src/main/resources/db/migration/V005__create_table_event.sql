CREATE TABLE events (
    id BIGINT NOT NULL AUTO_INCREMENT,
    Name VARCHAR(50) NOT NULL,
    Description VARCHAR(255) NULL,
    Address VARCHAR(255) NOT NULL,
    Time TIME NOT NULL,
    LocationId BIGINT NOT NULL,
    GroupId BIGINT NOT NULL,
    PRIMARY KEY (id),
        INDEX FK_events_location_idx (LocationId ASC) VISIBLE,
        INDEX FK_events_group_idx (GroupId ASC) VISIBLE,
            CONSTRAINT FK_events_location
                FOREIGN KEY (LocationId)
                REFERENCES locations (id)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
            CONSTRAINT FK_events_group
                FOREIGN KEY (GroupId)
                REFERENCES `groups` (id)
                ON DELETE CASCADE
                ON UPDATE CASCADE)
engine=InnoDB;
