CREATE TABLE `groups`(
    id BIGINT NOT NULL AUTO_INCREMENT,
    Name VARCHAR(50) NOT NULL,
    Description VARCHAR(255) NULL,
    Image VARCHAR(255) NULL,
    CategoryId BIGINT NOT NULL,
    UserId BIGINT NOT NULL,
    PRIMARY KEY (id),
    INDEX FK_groups_users_idx (UserId ASC) VISIBLE,
    INDEX FK_groups_categories_idx (CategoryId ASC) VISIBLE,
        CONSTRAINT FK_groups_users
            FOREIGN KEY (UserId)
            REFERENCES users (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
        CONSTRAINT FK_groups_categories
            FOREIGN KEY (CategoryId)
            REFERENCES categories (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)engine=InnoDB;
