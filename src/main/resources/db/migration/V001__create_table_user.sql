CREATE TABLE users(
    id BIGINT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    enabled TINYINT NOT NULL,
    role VARCHAR(45) NOT NULL,
    PRIMARY KEY (id))engine=InnoDB;